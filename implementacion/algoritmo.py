from dis import dis
from tabnanny import verbose
import matplotlib.pyplot as plt
import numpy as np

from datetime import datetime

from numpy.lib.function_base import disp

from impor_graph import Graph

XMAX = 1000 #Diemncion del lienzo horizontal
YMAX = 1000 #Dimencion del lienzo vertical

EPSILON = 0.05 # distancia minima entre dos nodos

#Se utiliza para evitar que dos nodos se aproximen demaciado
def avoid_collisions (i, coordenadas):
    cambios = []

    for j in range(i):
        dist = np.sqrt(np.square(coordenadas[i][0] - coordenadas[j][0]) + np.square(coordenadas[i][1] - coordenadas[j][1]))
        if dist < EPSILON:
            cambios += [j]
            direction_vector = (np.random.rand(), np.random.rand())
            direction_vector_op = (-direction_vector[0], -direction_vector[1])

			#Nuevas coordenadas para evitar superpocicion
            coordenadas[i][0] *= direction_vector[0]
            coordenadas[i][1] *= direction_vector[1]
            coordenadas[j][0] *= direction_vector_op[0]
            coordenadas[j][1] *= direction_vector_op[1]

    if cambios == [] :
        return
    else:
        for elem in cambios: 
            avoid_collisions(elem, coordenadas)
        avoid_collisions(i, coordenadas)

    return

### Plot del grafo ###
def plot_graph(cantVertices, coordenadas, aristas):
	
	#Imprime en el lienzo los vertices
	for i in range(0, cantVertices):
		plt.scatter(coordenadas[i][0],coordenadas[i][1])
	
	#Imprime en el lienzo las aristas
	for v,w in aristas: 
		plt.plot([coordenadas[v][0],coordenadas[w][0]], [coordenadas[v][1], coordenadas[w][1]])
	
	#Ejes al rededor del lienzo
	plt.xlim(0, XMAX)
	plt.ylim(0, YMAX)

	plt.draw()#Imprime sobre el lienzo
	return

def cleen_graph(wait = 0.2):
	plt.pause(wait)
	plt.cla()

def plot_safe(name, v):
	now = datetime.now()
	name = name + str(now.year) + str(now.month) + str(now.day) + str(now.hour) + str(now.minute) + str(now.second) + ".png" 

	if v:
		print("Nombre del archivo de salida: ", name)

	print(name)
	plt.savefig(name)
	return

### Inicializaciones ###
def pos_init(xMax, yMax, cantPuntos):
	puntos = np.zeros((cantPuntos,2))

	for i in range(0,cantPuntos):
		x,y = np.random.rand(2)

		puntos[i][0] = x * xMax/2 + xMax/4
		puntos[i][1] = y * yMax/2 + yMax/4
	
	return puntos

def inicialize_acumulators(n):
	return np.zeros((n,2))

### Calculo de fuerzas ###
def f_attraction(dist, k):
	return  np.square(dist) / k

def f_repultion(dist, k):
	return - np.square(k) / dist
	
def f_gravity(dist,k):
	return 0.1 * f_attraction(dist,k)

class LayoutGraph:
	def __init__(self, grafo, iters, refresh, temp, c1, c2, verbose=False, safe = True):
		"""
		Parámetros:
		grafo: grafo en formato lista
		iters: cantidad de iteraciones a realizar
		refresh: cada cuántas iteraciones graficar. Si su valor es cero, entonces debe graficarse solo al final.
		temp: incremento de la tempreratura por cada iteracion
		c1: constante de repulsión
		c2: constante de atracción
		verbose: si está encendido, activa los comentarios
		safe: guardado de una imagen tras la ejecucion
		"""

		# Guardo el grafo
		self.grafo = grafo

		# Inicializo estado
		self.centro = (XMAX/2, YMAX/2)	#Centro del lienzo
		self.posiciones = pos_init(XMAX, YMAX, self.grafo.cant_vertices)

		self.iters = iters		#cant iteraciones que se calcula
		self.verbose = verbose	#si/no plotea informacion
		self.safe = safe		#si/no guardar el grafo obtenido
		
		self.temperatureCTE = temp #Incremento de la tempreratura por cada iteracion
		self.refresh = refresh	   #Tasa de refresco del lienzo
		self.c1 = c1 * np.sqrt((YMAX * XMAX) / self.grafo.cant_vertices) #Constante Repulcion
		self.c2 = c2 * np.sqrt((YMAX * XMAX) / self.grafo.cant_vertices) #Constante Atraccion
		

	def layout(self):
		"""
		Aplica el algoritmo de Fruchtermann-Reingold para obtener (y mostrar)
		un layout
		"""
		if self.verbose:
			print("=== Modo verbose ON ===")
			print("Tamanio ventana:", "x=", XMAX, "y=", YMAX)
			print("iter:", self.iters)
			print("Tasa refresco:", self.refresh)
			print("CTE: C1=", self.c1, "C2", self.c2)
			print("safe:", self.safe)
			print(self.posiciones)
			print("Temperatura Inicial:", self.temperatureCTE)
			print("=== Modo verbose ON ===")
			
		self.temperature = 100.0
		
		plot_graph(self.grafo.cant_vertices, self.posiciones,self.grafo.aristas)
		
		#Comienzo del algoritmo
		for i in range(0, self.iters): 
			if (i % self.refresh == 0):
				print("#####",i,"#####")
				print("Temperatura:", self.temperature)
				for v in range(self.grafo.cant_vertices):
					print("vert: ", v, self.posiciones[v])        
			self.__step()
			self.__refresh(i)
		
		
		if self.safe:
			plot_safe(self.grafo.name, self.verbose)

	def __step(self):
		self.__acumuladores = inicialize_acumulators(self.grafo.cant_vertices)
		self.__compute_attraction_forces()
		self.__compute_repulsion_forces()
		self.__compute_gravity_forces()
		
		self.__update_positions()
		
		self.__update_temperature()
		
	
	def __refresh(self, i):
		if (i % self.refresh == 0):
			cleen_graph()
			plot_graph(self.grafo.cant_vertices, self.posiciones,self.grafo.aristas)

	def __compute_attraction_forces(self):
		for v,w in self.grafo.aristas:
			x = self.posiciones[v][0] - self.posiciones[w][0]
			y = self.posiciones[v][1] - self.posiciones[w][1]
						
			dist = np.sqrt(np.square(x) + np.square(y))

			modfa = f_attraction(dist, self.c2)

			fx = modfa * (-x) / dist
			fy = modfa * (-y) / dist

			self.__acumuladores[v][0] += fx
			self.__acumuladores[v][1] += fy
			self.__acumuladores[w][0] -= fx
			self.__acumuladores[w][1] -= fy
	
	def __compute_repulsion_forces(self):
		for v in range(0,self.grafo.cant_vertices):
			for w in range(0,self.grafo.cant_vertices):
				if (w != v):
					x = self.posiciones[v][0] - self.posiciones[w][0]
					y = self.posiciones[v][1] - self.posiciones[w][1]
					
					dist = np.sqrt(np.square(x) + np.square(y))
					
					modfa = f_repultion(dist, self.c1)

					fx = modfa * (-x) / abs(dist)
					fy = modfa * (-y) / abs(dist)

					self.__acumuladores[v][0] += fx
					self.__acumuladores[v][1] += fy
					self.__acumuladores[w][0] -= fx
					self.__acumuladores[w][1] -= fy
						
	def __compute_gravity_forces(self):
		for v in range(0,self.grafo.cant_vertices):
			x = self.posiciones[v][0] - self.centro[0]
			y = self.posiciones[v][1] - self.centro[1]
			dist = np.sqrt(np.square(x) + np.square(y))
			
			modfg = f_gravity(dist,self.c2)

			fx = modfg * (-x) / dist
			fy = modfg * (-y) / dist
			
			self.__acumuladores[v][0] += fx
			self.__acumuladores[v][1] += fy
		
	def __update_positions(self):
		for v in range(0,self.grafo.cant_vertices):
			f = (self.__acumuladores[v][0], self.__acumuladores[v][1])
			
			modulo = np.sqrt(np.square(f[0]) + np.square(f[1]))
			
			if modulo > self.temperature:
				f = (f[0]/modulo * self.temperature, f[1]/modulo * self.temperature)
				self.__acumuladores[v][0], self.__acumuladores[v][1] = f
				
			temp1 = self.posiciones[v][0] + self.__acumuladores[v][0]
			temp2 = self.posiciones[v][1] + self.__acumuladores[v][1]

			#Evitar que los vertices se salgan del liezo	
			if temp1 <= 0.0:	#Escape por izq
				self.posiciones[v][0] = 50
			elif temp1 <= XMAX:	#En el lienzo
				self.posiciones[v][0] = temp1
			else:				#Escapa por der
				self.posiciones[v][0] = XMAX - 50
			if temp2 <= 0.0:	#Escapa por debajo
				self.posiciones[v][1] = 50
			elif temp2 <= YMAX:	#En el lienzo
				self.posiciones[v][1] = temp2
			else:				#Escapa por arriba
				self.posiciones[v][1] = YMAX - 50

			avoid_collisions(v, self.posiciones)	#Evitar superpocicion de vertices

	def __update_temperature(self):
		self.temperature *= self.temperatureCTE
