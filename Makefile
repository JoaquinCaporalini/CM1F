#Facultad de Cienciasl Exactas, Ingenierias y Agrimensura, UNR
#Trabajo final complementos matematicos 1

#Alumno: 	Caporalini, Joaquin
#           Peralta Aguilera, Lautaro
#Docente:	Bianchi, Gabina

#fecha:		Abr 2022

PY = python3

info:
	@echo "FCEIA (UNR) Complementos matematicos I - trabajo final"
	@echo Alumno: 	Caporalini, Joaquin - Peralta Aguilera, Lautaro
	@echo Docente:  Bianchi, Gabina
	$(PY) implementacion/main.py -h

test_matplotlib:
	$(PY) tpfinal/plots/demo.py &
	$(PY) tpfinal/plots/demo2.py &

test:
	$(PY) implementacion/main.py --refresh=100 --iters=1000 tpfinal/grafos/K8.txt --safe

silence:
	$(PY) implementacion/main.py --refresh=50 --iters=1000 tpfinal/grafos/K5.txt --safe > log.txt

clean:
	@echo Borrado de las imagenes generadas
	@rm tpfinal/grafos/*.png
	@echo Borrado de log
	@rm log.txt
