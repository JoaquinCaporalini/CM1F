import argparse
from impor_graph import Graph 
from algoritmo import LayoutGraph

def main():
    # Definimos los argumentos de linea de comando que aceptamos
    parser = argparse.ArgumentParser()

    # Verbosidad, opcional, False por defecto
    parser.add_argument(
        '-v', '--verbose',
        action='store_true',
        help='Muestra mas informacion al correr el programa'
    )
    # Cantidad de iteraciones, opcional, 50 por defecto
    parser.add_argument(
        '--iters',
        type=int,
        help='Cantidad de iteraciones a efectuar',
        default=50
    )
    # Temperatura inicial
    parser.add_argument(
        '--temp',
        type=float,
        help='Temperatura inicial',
        default=0.95
    )
    # Taza de refresco
    parser.add_argument(
        '-r', '--refresh',
        type=int,
        help='cada cuántas iteraciones graficar. Si su valor es cero, entonces debe graficarse solo al final.',
        default=100.0
    )
    # Repulcion
    parser.add_argument(
        '--C1',
        type=float,
        help='constante de repulsión',
        default = 0.1
    )
    # Atracción
    parser.add_argument(
        '--C2',
        type=float,
        help='constante de atracción',
        default=5.0
    )
    # Archivo del cual leer el grafo
    parser.add_argument(
        'file_name',
        help='Archivo del cual leer el grafo a dibujar',
        default=""
    )

    # Atracción
    parser.add_argument(
        '-s', '--safe',
        action = 'store_true',
        help='Guarda el grafo terminado'
    )

    args = parser.parse_args()

    gr = Graph(args.file_name)
    layout = LayoutGraph(
        Graph(args.file_name),
        args.iters,
        args.refresh,
        args.temp,
        args.C1,
        args.C2,
        args.verbose,
        args.safe
    )
    layout.layout()
    return

if __name__ == '__main__':
    main()
