import numpy as np

class Graph:
    def __init__(self, nameFile):
        self.name = nameFile.split('.')[0]
        with open(nameFile, 'r') as f:
            self.cant_vertices = f.readline().strip() 

            #Errores comunes a la hora de generar un nuevo grafo
            #cantidad de vertices entera
            if not(self.cant_vertices.isnumeric()):
                raise ValueError("La cantidad de vertices debe ser entera.")
            #Correccion de tipo
            if type(self.cant_vertices) != type(1):
                self.cant_vertices = int(self.cant_vertices)
            #cantidad vertices negativa
            if (self.cant_vertices < 0):
                raise ValueError("La cantidad de vertices debe ser positiva.")

            #Abstraccion del nombre de los nodos (nombres -> numeros)
            name2pos = {}
            for i in range(0, self.cant_vertices):
                name2pos[str(f.readline().strip())] = i

            self.aristas = []

            end = True
            while end:
                line = f.readline().strip()
                if line == 'end' or line == '':
                    end = False
                else:
                    line = line.split()

                    #filtrado de errores
                    if (len(line) != 2):
                        raise ValueError("Las aristas deben unir 2 vertices")
                    try:
                        origen = name2pos[line[0]]
                    except:
                        raise ValueError("El vertice de origen no pertenecen al Grafo")
                    try:
                        destino = name2pos[line[1]]
                    except:
                        raise ValueError("El vertice de destino no pertenecen al Grafo")
                    
                    self.aristas.append((origen, destino))

###### Solo para test ######
def main():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        'file_name',
        help='Archivo del cual leer el grafo a importar'
    )

    gr = Graph(parser.parse_args().file_name)
    print(gr.name)
    print(gr.aristas)
    print(gr.cant_vertices)

if __name__ == '__main__':
    import argparse
    main()
