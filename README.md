# Complementos Matemáticos 1 - Final
Alumno: 	Caporalini, Joaquin
            Peralta Aguilera, Lautaro
Docente:	Bianchi, Gabina

fecha:		Abr 2022

## Modo de uso

Para utilizar la implementación del algoritmo de Fruchterman-Reingold realizado desde python se debe utilizar ejecutando el siguiente comando por consola: 

```
python3 main.py [GRAFO.txt]
```

Dentro de la carpeta “implementacion”. Se asume que los grafos que toma el programa siguen el formato dado anteriormente como ejemplo por parte de los docentes. Existen algunos grafos de ejemplo definidos en la carpeta “tpfinal/grafos”


Además se disponen distintas banderas que podemos ir sumando para modificar el comportamiento del algoritmo a través de modificar las constantes de ejecución tales como las constantes de repulsión y atracción, temperatura, iteraciones entre un par más. 

Para obtener más información se puede consultar a través del comando:  

```
python3 main.py main.py -h
```

Si se desea se puede correr con un caso de prueba definido por nuestra parte, si se dispone de makefile, corriendoló desde la carpeta raíz del proyecto con 

```
make test
```

obtendremos un ejemplo de ejecución del proyecto. Además en el make tenemos otras dos opciones:
* “test_matplotlib” que prueba la que la librería de de impresiones gráficas esté instalada y funcionando,
* “silence” que es otra ejecución distinta con otro grafo distinto, sin demasiadas banderas de para el testeo,
* “clean” limpia los posibles archivos generados por el programa,
* “info” que muestra información de la llamada al algoritmo.

## Información del desarrollo

El desarrollo del trabajo se realizó en Ubuntu 20.04 y utilizando python 3.8

# Git

Para el seguimiento del desarrollo se utilizó gitlab

link al repo: https://gitlab.com/JoaquinCaporalini/CM1F


